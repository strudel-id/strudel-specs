# Strudel ID Caveats

Studel IDs are intended to be long enough to remain unique for years but short
enough to be pronounceable.  This tension leads to compromises.  The
specification attempts to make these compromises in the best possible ways.


## A. Instances Cannot Share a Partition Simultaneously

Partitioning is what allows Strudel IDs to be generated without the need for a
centralized coordinator.  However, to make this work, all instances generating
IDs simultaneously must guarantee they are operating on distinct partitions.  If
two or more instances are generating with the same partition, there is a good
chance that duplicate IDs will result.

This is more likely to cause an issue in environments where instances scale up
or down on demand, such as with cloud and serverless providers.

### Possible Solutions

A solution if the list of instances is fixed is to assign a partition via
configuration (e.g. an environment variable).

An alternative take - applicable for cloud providers - would be to assign a
unique partition to a specific instance through labels or tags (e.g. Kubernetes
cluster labels).

Another solution would be to keep the ID generation decentralized but allow a
centralized service to maintain a pool of partitions and dole out an available
one to instances as they come online.


## B. Instances Should Respect the Three-Second Restart Interval

A design tenant of Strudel IDs is that they can be generated without state
(other than a monotonic clock).  No database, file or centralized key/value
store is required.  However, the time portion of the ID is represented by a
value between 0 and 28799, the number of 3-second periods in a day.  If an
instance were to restart more than once every three seconds, there could be no
guarantee that the same ID would not be regenerated.

### Possible Solutions

Three seconds was chosen so that the time of day can be represented in 15 bits
(encoded in three characters).  By altering the specification to allow for 20
bits (encoded in four characters), the restart interval could be reduced to 1/10
second.

## C. Studel IDs Can Repeat After 32 Years

As currently specified, the year of generation is represented as the current
year modulo 32 (so that it can be encoded in a single character).  So an ID
generated in 2016 could repeat when generated in 2048 (assuming the same
partition, month, day of month, time period and sequence).

### Possible Solutions

By altering the specification to encode the year modulus in 10 bits instead of 5
(two characters instead of one), Studel IDs could live for over a thousand
years.


## D. Clocks Must Be Monotonic

In order for IDs not to repeat and to represent the true date and time they were
generated, the clock source must be monotonic.  For instance, a clock that
reverts an hour for Daylight Saving Time (DST) would not be correct as there are
two 1:00 - 2:00 AM periods on the night that DST begins.

Using a clock that represents UTC has the advantages of being monotonic and
available in a wide array of programming languages.


## E. Timestamps are UTC

Strudel IDs store date and time values in UTC.  If it's desirable to report
these values in local time, the calling code is responsible for performing such
conversions.

### Possible Solutions

By altering the specification to add an additional 5 bits (one character), one
of 32 possible timezones could be encoded in the ID to specify the date and time
as local and not UTC.


## Additional Notes

The specification is not vulnerable to the [Year 2038 problem].  The `Clock`
interface expects to return a 64-bit representation of the number of seconds
since the start of the [Unix Epoch] (00:00:00 UTC on 1 January 1970), thus
avoiding the rollover issue that will befall systems that use 32 bits.

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/by.svg" /></a>
&copy; 2020 Evan P. Mills.
<br/>
The Strudel ID specifications are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.


[Year 2038 problem]: https://en.wikipedia.org/wiki/Year_2038_problem

[Unix Epoch]: https://en.wikipedia.org/wiki/Unix_time