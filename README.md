# Strudel ID

> Unique stateless identifiers that are **S**hort, **T**ranslucent,
> **R**eadable, **U**nambiguous, **D**ecentralized with **E**phemeral
> **L**ifetimes.


## Introduction

The ability to generate a unique ID is a very common requirement of software
systems, e.g. for assigning database primary keys or transaction IDs.

[UUID]s are probably the most common unique IDs today.  There are some
interesting alternatives, including Twitter's [Snowflake] and [ulid]s.

Because a design goal of most unique ID systems is to guarantee that IDs *never*
repeat, they require many bits to be encoded, generally 128 or 160.

And while this can be a great attribute, it does lead to IDs that are long and
difficult to communicate verbally or in typed form.

Strudel IDs take a different approach.  A few constraints are placed on the
system in order to produce IDs that are virtually unique yet short and easy to
speak and type.

In addition, Strudel IDs do not require persistent state in order to be be
generated.  By partitioning the ID value space, IDs can be generated
simultaneously without a centralized broker.  The [SPECIFICATION] allows for up
to 11 million IDs to be generated per second.

A monotonic clock source that can tell how many seconds have elapsed since the
start of the [Unix Epoch] is a requirement.

The major constraints are that there can be up to 32 instances generating IDs at
any given time and that each running instance must be assigned a unique number.
Finally, IDs have a 32-year lifespan after which they may begin repeating.

See [CAVEATS] for a more complete description of Strudel ID's limitations and
gotchas.

If those limitations are acceptable, you can be rewarded with unique IDs that
are generally between seven and nine characters long, easy to say, live to a
ripe old age without repeating and that do not require non-volatile storage, a
centralized broker, eventual consistency or OS- or architecture-specific code.

**In other words, Strudel IDs provide the ability to generate many unique IDs
quickly, in parallel and with minimal dependencies.**

To sum up the acronym, Strudel IDs are:
- **S**hort
  - Generally seven to nine characters
- **T**ranslucent
  - We can introspect an ID to determine when it was generated and by what
    instance (within three seconds and by inferring a year)
- **R**eadable
  - By encoding in ZBase-32, we generate IDs that are easy to verbalize
- **U**nambiguous
  - IDs do not duplicate even across partitioned producers
- **D**ecentralized
  - No central broker or shared storage is required
- **E**phemeral <br/>**L**ifetimes
    - By not requiring IDs never to repeat, we can produce IDs that are shorter


## Configuration

Refer to [CONFIGURATION] for an explanation of how Strudel IDs are serialized
and how to tailor their output format.


## Specification

The specification for Strudel IDs and their serialization can be found in
[SPECIFICATION].


[CAVEATS]: docs/CAVEATS.md

[CONFIGURATION]: docs/CONFIGURATION.md

[SPECIFICATION]: docs/SPECIFICATION.md

[docs/CAVEATS]: docs/CAVEATS.md

[Unix Epoch]: https://en.wikipedia.org/wiki/Unix_time

[UUID]: https://en.wikipedia.org/wiki/Universally_unique_identifier

[Snowflake]: https://github.com/twitter-archive/snowflake

[ulid]: https://github.com/ulid/spec

[generating]:
java/examples/src/main/java/com/example/strudelid/ExampleGenerator.java

[inspecting]:
java/examples/src/main/java/com/example/strudelid/ExampleInspector.java

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/by.svg" /></a>
&copy; 2020 Evan P. Mills.
<br/>
The Strudel ID specifications are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
