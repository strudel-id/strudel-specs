# Strudel ID

## Assets

Logos, screenshots and other assets for Strudel IDs.

### Master

The master copy is a [Pixelmator Pro][pxm] file named [strudel-id.pxm][master].

All changes should originate there.

### Color Themes

There are six color themes:

Preview           | Name         | Designation
------------------|--------------|------------
![][charcoal]     | Charcoal     | *Specifications*
![][grapefruit]   | Grapefruit   | *Java*
![][hunter_green] | Hunter Green | *Group*
![][lilac]        | Lilac        | *Aggregate*
![][linen]        | Linen        | *Assets*
![][sky]          | Sky          | *Dart*


[pxm]: https://www.pixelmator.com/pro/
[master]: strudel-id.pxm
[charcoal]: ./strudel-id-charcoal/res/mipmap-mdpi/strudel-id-charcoal.png
[grapefruit]: ./strudel-id-grapefruit/res/mipmap-mdpi/strudel-id-grapefruit.png
[hunter_green]: ./strudel-id-hunter-green/res/mipmap-mdpi/strudel-id-hunter-green.png
[lilac]: ./strudel-id-lilac/res/mipmap-mdpi/strudel-id-lilac.png
[linen]: ./strudel-id-linen/res/mipmap-mdpi/strudel-id-linen.png
[sky]: ./strudel-id-sky/res/mipmap-mdpi/strudel-id-sky.png