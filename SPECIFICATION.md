# Strudel ID Specification

A Strudel ID can be represented in a multitude of ways:

- As a *logical structure* consisting of a date and time period, **partition**
  and **sequence**
- As a *packed structure*, a 64-bit integer whose bits represent a logical
  Strudel ID
- As a *ZBase-32 encoded string* whose characters represent a logical Strudel ID


## Clock

Generating Strudel IDs requires a monotonic clock that can return the current
number of seconds since the start of the [Unix Epoch].  Timestamps returned from
such a clock should be at least 64 bits to avoid the so-called Year 2038
problem.  Timestamps are always interpreted as UTC.

See *Clocks Must Be Monotonic* and *Additional Notes* in [CAVEATS].


## Binary Layout

A Strudel ID is packed as an unsigned 64-bit integer (let's refer to this as a
*long*). By employing a common layout, Strudel IDs can be serialized and
deserialized easily in any supported language.

The long is divided into 12 5-bit **segments** with the most significant four
bits unused.

Why five bits?  Because with five bits you can encode 32 unique values and
32-bit character encodings such as ZBase-32 make great building blocks for
forming short IDs that are unambiguous when spoken.

Here is how the data is arranged within the long:

![Binary Layout](assets/diagrams/binary-layout.svg)

  Label  | Description
:-------:|------------
   x     | Unused
   Q4    | Sequence - MSS
   Q3    | Sequence
   Q2    | Sequence
   Q1    | Sequence
   Q0    | Sequence - LSS
   P     | Partition
   T2    | Time Period - MSS
   T1    | Time Period
   T0    | Time Period - LSS
   D     | Day of month
   M     | Month of year
   Y     | Year modulo 32

- *MSB/LSB - Most/Least Significant Bit*
- *MSS/LSS - Most/Least Significant Segment*


## Converting a Long to a Strudel ID

Given a Strudel ID that was packed into a long, here are the steps for decoding
that long back into a logical Strudel ID:

1. Take the long value and mask it with the **segment mask** (`0x1F`). This is
   **Y**, the year modulo 32. The actual year must be inferred] from **Y** (see
   *Year Inference*, below).
2. Shift the long five bits to the right.
3. Mask the long with the segment mask.  This is **M**, the month of the year.
   Month values are 1-12 inclusive. For example, `9` would be decoded as
   September.
4. Shift the long five bits to the right.
5. Mask the long with the segment mask.  This is **D**, the day of the month.
   Day values are 1-31 inclusive.  For example, 17 would be decoded as the 17th
   of the month. 
6. Shift the long five bits to the right.
7. Mask the long with the segment mask. This is **T0**, the least significant
   segment of the time period.
8.  Shift the long five bits to the right.
9.  Mask the long with the segment mask. This is **T1**.
10. Shift the long five bits to the right.
11. Mask the long with the segment mask. This is **T2**, the most significant
    segment of the time period.
12. Calculate **T** = 1024∙T2 + 32∙T1 + T0. This is the time period, one of the
    28,800 three-second time periods in a day.
13. Shift the long five bits to the right.
14. Mask the long with the segment mask. This is **P**, the partition. Partition
    values are 0-31 inclusive.  For example `12` would be decoded as partition
    12.
15. Shift the long five bits to the right.
16. Mask the long with the segment mask. This is **Q0**, the least significant
    segment of the sequence.
17. Shift the long five bits to the right.
18. Mask the long with the segment mask. This is **Q1**.
19. Shift the long five bits to the right.
20. Mask the long with the segment mask. This is **Q2**.
21. Shift the long five bits to the right.
22. Mask the long with the segment mask. This is **Q3**.
23. Shift the long five bits to the right.
24. Mask the long with the segment mask. This is **Q4**.
25. Calculate **Q** = 1048576∙**Q4** + 32768∙**Q3** + 1024∙**Q2** + 32∙**Q1** +
    **Q0**.  This is the sequence, starting at zero and incrementing by one for
    each ID whose year, month, day and time period match those of the preceding
    ID.
26. Given **T**, calculate the hour, minute and second.  See *Converting Between
    Time Period and Hour/Minute/Second*, below.

You have now gathered all the logical parts of a Strudel ID: the inferred year,
month **M**, day of month **D**, hour, minute, second, partition **P** and
sequence **Q**.

The timestamp (i.e. the number of seconds since the start of the Unix Epoch) can
be calculated from the inferred year, month, day, hour, minute and second using
a standard function available in nearly every programming language.  Here's an
example in Java:

```java
long timestamp = LocalDateTime.of(year, month, day, hour, minute, second).toEpochSecond(ZoneOffset.UTC);
```


## Converting a Strudel ID to a Long

Given a Studel ID, consisting of a timestamp, a sequence and a partition.

The year, month, day, hour, minute and second can be derived from the timestamp
using a standard function available in nearly every programming language. Here's
an example in Java:

```java
LocalDateTime d = LocalDateTime.ofEpochSecond(timestamp, 0, ZoneOffset.UTC);

int year = d.getYear();
int month = d.getMonth().getValue();
int day = d.getDayOfMonth();
int hour = d.getHour();
int minute = d.getMinute();
int second = d.getSecond();
```

Here are the steps to encoding it as a long:

- Let the long = 0
- Let *q* = **Q**
- While *q* is not zero:
  - Let *k* = a [bitwise AND] of *q* and the segment mask.
  - Perform a bitwise OR of **L** with *k*.
  - Shift *q* five bits to the right.
  - Shift the long five bits to the left.
- Perform a bitwise OR of **L** with **P**
- Shift the long five bits to the left.
- Convert the timestamp into its year, month **M**, day **D**, hour, minute and
  second.
- Calculate **Y** as the year modulo 32.
- Calculate **T** from the hour **h**, minute **m** and second **s**.  See
  *Converting Between Time Period and Hour/Minute/Second*, below.
- Let *t* = **T**
- Repeat three times:
  - Let *k* = a bitwise AND of *t* and the segment mask.
  - Perform a bitwise OR of **L** with *k*.
  - Shift *t* five bits to the right.
  - Shift the long five bits to the left.
- Shift the long five bits to the left.
- Perform a bitwise OR of **L** with **D**
- Shift the long five bits to the left.
- Perform a bitwise OR of **L** with **M**
- Shift the long five bits to the left.
- Perform a bitwise OR of **L** with **Y**


## Converting Between Time Period and Hour/Minute/Second

As mentioned, each day is divided into 28,800 three-second time periods. This
time resolution was selected because time periods can be expressed in 15 bits
and therefore three ZBase-32 characters.

With a time period of three seconds and 60 seconds per minute, there are
therefore 20 such periods per minute.

To convert from a time period **T** and time periods per minute **R** to hours
**h**, minutes **m** and seconds **s**:

1. Let *t* = **T**
2. Let hour *h* = truncate(*t*∕60∙*R*)
3. Subtract *h*∙60∙*R* from *t*
4. Let minute *m* = truncate(*t*∕*R*)
5. Subtract *m*∙*R* from *t*
6. Let second *s* = *t*

To convert from hour **h**, minute **m**, second **s** and time periods per
minute **R** to time period **T**:

1. Let *t* = *s*∕*R*
2. Add *R*∙*m* to *t*
3. Add 60∙*R*∙*h* to *t*
4. Let **T** = *t*


## Year Inference

Because the year is stored modulo 32, that value is not sufficient to identify
the actual year the ID was generated with absolute certainty.  For example, a
value of `3` could mean 2019, 2051 or even 1987.

The specification does not define an authoritative algorithm for inferring the
year.

A reasonable approach would be find the year whose value modulo 32 occurs the
most recently relative to the current year.  For example, if the current year is
2019, then a value of `6` would be interpreted as `1990`.


## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/by.svg" /></a>
&copy; 2020 Evan P. Mills.
<br/>
The Strudel ID specifications are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.


[Unix Epoch]: https://en.wikipedia.org/wiki/Unix_time

[CAVEATS]: CAVEATS.md

[bitwise OR]: https://en.wikipedia.org/wiki/Bitwise_operation#OR

[bitwise AND]: https://en.wikipedia.org/wiki/Bitwise_operation#AND