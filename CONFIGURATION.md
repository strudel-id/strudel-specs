# Strudel ID Configuration

How Strudel IDs are created, saved as binary data and converted to
human-readable strings is driven by configuration.  This configuration is
divided into two categories:

- Layout
- Format

## Layout

Layout drives how a Strudel ID is represented in binary format.  It is crucial
that this configuration match what is detailed in the [SPECIFICATION].  In this
respect, layout is less of configuration and more of decoupling of the
specification from the (slightly) more generic engine that implements it.

A change in the layout, while possible, could have strong ramifications on the
implementation.  For instance, if the segment size were increased from five to
six bits in the layout, a 64-bit integer would not be large enough to
accommodate all of a Strudel ID's fields.  Perhaps a byte array or similar would
then be needed.

So, altering the layout is not something that can be done easily but is isolated
in a way that it could be done more readily than a complete rewrite would
require.  Of course, any change to the layout would require changes in the
documentation of the specification.

For completeness, here is a list of the layout parameters:

Parameter               | Derived    | Description
----------------------- |:----------:| -----------
Bits Per Segment        | No         | Number of bits comprising a segment
Max Segment Value       | Yes        | Number of values that can be represented in a segment
Segment Mask            | Yes        | The bit mask required to constrain a value to a segment
Segments Per Partition  | No         | How many segments comprise the partition
Segments Per Time       | No         | How many segments comprise the day's time period
Segments Per Year       | No         | How many segments comprise the modulus year
Segments Per Month      | No         | How many segments comprise the month of year
Segments Per Day        | No         | How many segments comprise the day of month
Segments Per Date       | Yes        | How many segments comprise the date
Time Resolution         | No         | How many seconds comprise a time period
Time Periods Per Minute | Yes        | Number of time periods in a minute
Time Periods Per Day    | Yes        | Number of time periods in a day

It's required that the time resolution divide evenly into a minute.  In
addition, the time resolution should be small enough to represent a reasonably
short restart interval for an instance but large enough so that the time periods
per day can be represented in segments-per-time.


## Format

Format drives how a Strudel ID is shown to a human and is much easier to alter
than layout.  And, in fact, format can be strongly tied to layout.  For
instance, the segment size of five bits in the layout means that a 32-character
alphabet should be used when translating Strudel IDs into readable strings.

Parameter  | Type    | Description
---------- |:-------:| -----------
Upper Case | Boolean | If true, the ID is converted to upper case
Reverse    | Boolean | If true, the order of the ID's characters are reversed
Separator  | String  | String to insert between groups of the ID's characters 
Splitter   | String  | Name of the splitter function to apply

### Splitter

The splitter is a function that takes an ID string and divides it into groups of
characters.  Between each group, the separator is inserted.

To maintain interoperability between different Strudel ID implementations, each
implementation should define the following standard splitter:

Splitter | Description                                      | Example
-------- | ------------------------------------------------ | -------------------------
EVERY3   | Divides the ID's characters into groups of three | `YBNDRFG8` ➔ `YBN DRF G8`

### Default Format

While many combination of formats are possible, again to support
interoperability, a default format is defined:

Parameter  | Value
---------- | -----
Upper Case | True
Reverse    | False
Separator  | Single Space
Splitter   | `EVERY3`

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/by.svg" /></a>
&copy; 2020 Evan P. Mills.
<br/>
The Strudel ID specifications are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.


[SPECIFICATION]: SPECIFICATION.md
